from django.shortcuts import render
from django.views import View
from django.views.generic import TemplateView

from myapp.models import Cat


# Create your views here.
class CatListView(View):
    def get(self, request):
        context = {'cats': Cat.objects.all()}
        return render(request, "index.html", context)

